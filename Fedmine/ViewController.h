//
//  ViewController.h
//  Fedmine
//
//  Created by Mohit Gupta on 25/07/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"

@interface ViewController : UIViewController<UIScrollViewDelegate>{
    
    IBOutlet UIPageControl *pageControl,*pageControl1;
    IBOutlet UIScrollView *scrollView;
    IBOutlet CardView *scrollView1;
}

-(IBAction)changePage:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarightButton;
@end

