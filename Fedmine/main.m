//
//  main.m
//  Fedmine
//
//  Created by Mohit Gupta on 25/07/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
