

#import "forgetVC.h"
#import <Foundation/Foundation.h>


@interface forgetVC ()

@end

@implementation forgetVC

#pragma mark - ViewController lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    
     _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 600);
    [_activityIndicatorView setHidden:true];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)RecoverPwdClick:(id)sender{
    
    if ([self.txtEmail.text  isEqualToString: @""] && [self.txtUsername.text  isEqualToString: @""]){
        [self showMessage:@"Error!" withTitle:@"Please Enter Correct username or Email"];
    }
    
    else{
        [self forgetAPICall];
    }
}


-(IBAction)backClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)forgetAPICall{
    
     _activityIndicatorView.transform = CGAffineTransformMakeScale(2.0, 2.0);
    [_activityIndicatorView setHidden:false];
    [_activityIndicatorView startAnimating];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"902dd746-36c8-a4f4-18b1-6055fb781383" };
    
    NSDictionary *parameters = @{ @"username": _txtUsername.text,
                                  @"email": _txtUsername.text};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://54.225.94.187/fedconnect_bizopps/api/forgot_pass"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"%@", httpResponse);
        
        
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSData * responseData = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self showMessage:[jsonDict valueForKey:@"error_msg"] withTitle:@""];
            if (error) {
                NSLog(@"%@", error);
                
            } else {
                NSLog(@"requestReply: %@", jsonDict);
            }
            _activityIndicatorView.transform = CGAffineTransformIdentity;
            [self.activityIndicatorView stopAnimating];
            [_activityIndicatorView setHidden:true];

        });
        
        
        
    }];
    [dataTask resume];
}

-(void)showMessage:(NSString*)message withTitle:(NSString *)title{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
     if (_txtUsername == textField){
        
        [_txtUsername resignFirstResponder];
    }
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
    
}

@end
