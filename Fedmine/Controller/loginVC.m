 
#import "loginVC.h"
#import <Foundation/Foundation.h>
#import "AlertListVC.h"
#import "changePasswordVC.h"
#import "DeleteCompanyVC.h"
#import "StatisticVC.h"
#import "RefferalVC.h"
#import "ResearchVC.h"
#import "registrationVC.h"
#import "forgetVC.h"

#import "SWRevealViewController.h"


@interface loginVC ()

@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end

@implementation loginVC

#pragma mark - ViewController lifecycle methods

/**
 *  All the HTTP request calling methods call inside this
 */


- (void)viewDidLoad {
    [super viewDidLoad];

    [_activityIndicatorView setHidden:true];
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 600);
    
}

-(IBAction)SignInClick:(id)sender{
    
    if ([self.txtUsername.text isEqualToString:@""]) {
        [self showMessage:@"Filled login credentials" withTitle:@"Error"];
    } else {
        [self loginAPICall];
    }
    
    
    
    //[self movetodashboard];
}


-(IBAction)ForgetPasswordClick:(id)sender{
    
    forgetVC *gotoYourClass = [self.storyboard instantiateViewControllerWithIdentifier:@"forgetVC"];
    [self.navigationController pushViewController:gotoYourClass animated:YES];
    
}


-(IBAction)sighUPClick:(id)sender{
    
    registrationVC *gotoYourClass = [self.storyboard instantiateViewControllerWithIdentifier:@"registrationVC"];
    [self.navigationController pushViewController:gotoYourClass animated:YES];
}


-(void)loginAPICall{
    
    
    _activityIndicatorView.transform = CGAffineTransformMakeScale(2.0, 2.0);
    
    [_activityIndicatorView setHidden:false];
    [self.activityIndicatorView startAnimating];
    
    
    
    NSDictionary *headers = @{@"content-type": @"application/json",@"cache-control": @"no-cache", @"postman-token": @"902dd746-36c8-a4f4-18b1-6055fb781383"};
    
    NSDictionary *parameters = @{@"username": _txtUsername.text,
                                  @"password": _txtpwd.text };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://54.225.94.187/fedconnect_bizopps/api/login"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"%@", httpResponse);
        
        
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSData * responseData = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                
                NSLog(@"%@", error);
                [self showMessage:@"" withTitle:@""];
                
            } else {
                
                
                NSLog(@"requestReply: %@", jsonDict);
                
                
                [[NSUserDefaults standardUserDefaults] setValue:[[jsonDict valueForKey:@"id"] stringValue] forKey:@"id"];
                
                [[NSUserDefaults standardUserDefaults] setValue:[[jsonDict valueForKey:@"username"] stringValue] forKey:@"username"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                
                [self movetodashboard];
                
                
                
            }
            _activityIndicatorView.transform = CGAffineTransformIdentity;
            [self.activityIndicatorView stopAnimating];
            [_activityIndicatorView setHidden:true];
            
        });
        
      }];
    [dataTask resume];
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (_txtUsername == textField){
        
        [self.txtpwd becomeFirstResponder];
    }
    else if (_txtpwd == textField){
        
        [self.txtpwd resignFirstResponder];
    } 
    return YES;
}


-(void)movetodashboard{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
//    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"tabBarcontroller"];
//
//    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    

    SWRevealViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    
    
     [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];

}


-(void)showMessage:(NSString*)message withTitle:(NSString *)title{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
    
}


@end
