

#import <UIKit/UIKit.h>

@interface registrationVC : UIViewController<UITextFieldDelegate>

@property(nonatomic, retain) IBOutlet UITextField *txtName;
@property(nonatomic, retain) IBOutlet UITextField *txtEmail;
@property(nonatomic, retain) IBOutlet UITextField *txtPhone;
@property(nonatomic, retain) IBOutlet UITextField *txtSubject;
@property (weak, nonatomic) IBOutlet UITextView *txtAddress;


@property(nonatomic, retain) IBOutlet UITextField *txtCity;
@property(nonatomic, retain) IBOutlet UITextField *txtState;
@property(nonatomic, retain) IBOutlet UITextField *txtCountry;
@property(nonatomic, retain) IBOutlet UITextField *txtzipcode;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
