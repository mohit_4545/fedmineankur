//
//  changePasswordVC.h
//  Fedmine
//
//  Created by Mohit Gupta on 22/08/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface changePasswordVC : UIViewController

@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property(nonatomic, retain) IBOutlet UITextField *txtoldpwd;
@property(nonatomic, retain) IBOutlet UITextField *txtpwd;
@property(nonatomic, retain) IBOutlet UITextField *txtconfirmpwd;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;


@end
