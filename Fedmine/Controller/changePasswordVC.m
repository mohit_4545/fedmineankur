//
//  changePasswordVC.m
//  Fedmine
//
//  Created by Mohit Gupta on 22/08/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import "changePasswordVC.h"
#import <Foundation/Foundation.h>
#import "SWRevealViewController.h"

@interface changePasswordVC ()

@end

@implementation changePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ){
        
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 600);
    [_activityIndicatorView setHidden:true];
    //[self changePasswordAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






-(void)changePasswordAPI{
    
    _activityIndicatorView.transform = CGAffineTransformMakeScale(2.0, 2.0);
    [_activityIndicatorView setHidden:false];
    
    NSDictionary *headers = @{ @"content-type": @"application/json", @"cache-control": @"no-cache", @"postman-token": @"902dd746-36c8-a4f4-18b1-6055fb781383" };

    NSString *username = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
    NSDictionary *parameters = @{@"username":username,@"new_password":_txtpwd.text, @"old_password":_txtoldpwd.text};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://54.225.94.187/fedconnect_bizopps/api/change_pass"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            NSLog(@"%@", error);
            [self showMessage:@"" withTitle:@""];
            
        } else {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
            
            
            NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            NSData * responseData = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            NSLog(@"requestReply: %@", jsonDict);
            
            
            
        }
        
        _activityIndicatorView.transform = CGAffineTransformIdentity;
        [self.activityIndicatorView stopAnimating];
        [_activityIndicatorView setHidden:true];
        
    }];
    [dataTask resume];
}

-(void)showMessage:(NSString*)message withTitle:(NSString *)title{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
