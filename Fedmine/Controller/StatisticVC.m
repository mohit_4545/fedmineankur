//
//  StatisticVC.m
//  Fedmine
//
//  Created by Mohit Gupta on 27/08/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import "StatisticVC.h"

@interface StatisticVC ()

@end

@implementation StatisticVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_activityIndicatorView setHidden:true];
    [self getStatisticAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getStatisticAPI1{
    
   
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"d0f59df7-d59f-eb17-5c13-d8447f47b170" };
    
    NSData *postData = [[NSData alloc] initWithData:[@"{“user_id”:”61”}" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://54.225.94.187/fedconnect_bizopps/api/get_statistics"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    
    [request setHTTPMethod:@"POST"];
    
    [request setAllHTTPHeaderFields:headers];
    
    [request setHTTPBody:postData];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            NSLog(@"%@", error);
            
        } else {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            NSLog(@"%@", httpResponse);
            
            NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            NSData * responseData = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            NSLog(@"requestReply: %@", jsonDict);
            
        }
        
    }];
    
    [dataTask resume];
}




-(void)getStatisticAPI{
    
    
    _activityIndicatorView.transform = CGAffineTransformMakeScale(2.0, 2.0);
    [_activityIndicatorView setHidden:false];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"902dd746-36c8-a4f4-18b1-6055fb781383" };
    
    
    
    
    
    NSDictionary *parameters = @{@"user_id":@"61"};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://54.225.94.187/fedconnect_bizopps/api/get_statistics"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            NSLog(@"%@", error);
            [self showMessage:@"" withTitle:@""];
            
            
        } else {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
            
            
            NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            NSData * responseData = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            NSLog(@"requestReply: %@", jsonDict);
            
            
            
        }
        
        _activityIndicatorView.transform = CGAffineTransformIdentity;
        [self.activityIndicatorView stopAnimating];
        [_activityIndicatorView setHidden:true];
        
    }];
    [dataTask resume];
}

-(void)showMessage:(NSString*)message withTitle:(NSString *)title{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
