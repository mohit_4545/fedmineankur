//
//  MenuViewController.m
//  Fedmine
//
//  Created by Mohit Gupta on 26/10/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import "MenuViewController.h"
#import "DTCustomColoredAccessory.h"
#import "customizeCell.h"
#import "SWRevealViewController.h"
#import "loginVC.h"
#import "ResearchVC.h"
#import "changePasswordVC.h"
#import "ViewController.h"
#import "getCompaniesVC.h"

#import "CompititorVC.h"


@interface MenuViewController (){
    
    NSArray *Arr;
    NSArray *imgArr;
    NSArray *categoaryArr;
    
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    categoaryArr = [[NSArray alloc]initWithObjects:@"Home",@"MyFedmine",@"FedConnect",@"Change Password",@"Logout", nil];
    imgArr = [[NSArray alloc]initWithObjects:@"ic_home_white",@"ic_flag_white",@"ic_compare_arrows_white",@"ic_security_white",@"ic_exit_to_app_white", nil];
    
    Arr = [[NSArray alloc]initWithObjects:@"MyCompanies",@"MyCompetitors",@"MyContractingOffices",@"MyContacts",@"My FederalOpportunities",@"MyGrants",@"MyFaads",@"MyResearch",@"My Refereal", nil];
    
    if (!expandedSections){
        
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section{
    
    if (section == 1) return YES;
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    // Return the number of sections.
    return [categoaryArr count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return [Arr count]; // return rows when expanded
        }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    customizeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[customizeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    // Configure the cell...
    cell.Label.textColor = [UIColor whiteColor];
    
    
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            // first row
            cell.Label.text = [categoaryArr objectAtIndex:indexPath.section]; // only top row showing
             cell.thumbnailImageView.image = [UIImage imageNamed:[imgArr objectAtIndex:indexPath.section]];
            
            if ([expandedSections containsIndex:indexPath.section])
            {
                cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeUp];
            }
            else
            {
                cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeDown];
            }
        }
        else
        {
            // all other rows
            cell.Label.text = Arr[indexPath.row];
            cell.thumbnailImageView.image = nil;
            cell.accessoryView = nil;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    else{
        
        cell.accessoryView = nil;
        cell.Label.text = [categoaryArr objectAtIndex:indexPath.section];
        cell.thumbnailImageView.image = [UIImage imageNamed:[imgArr objectAtIndex:indexPath.section]];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    if ([self tableView:tableView canCollapseSection:indexPath.section]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ResearchVC *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ResearchVC"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
        [navController setViewControllers: @[rootViewController] animated: YES];
        [self.revealViewController setFrontViewController:navController];
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    }
    
    if (indexPath.section == categoaryArr.count-1) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        loginVC *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
        
    } else if(indexPath.section == 1) {
        [self expendCollapse:tableView :indexPath];
    }
    
    else if (indexPath.section == categoaryArr.count-2){
        //[self customFunctionForMoveScreen:@"changePasswordVC"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        changePasswordVC *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"changePasswordVC"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
        [navController setViewControllers: @[rootViewController] animated: YES];
        [self.revealViewController setFrontViewController:navController];
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        
    }
    else if (indexPath.section == 0){
        //[self customFunctionForMoveScreen:@"changePasswordVC"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
        [navController setViewControllers: @[rootViewController] animated: YES];
        [self.revealViewController setFrontViewController:navController];
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        
    }
    
    
    
}

-(void)expendCollapse:(UITableView *)tableView :(NSIndexPath *)indexPath{
    
    if ([self tableView:tableView canCollapseSection:indexPath.section]){
        
        if (!indexPath.row){
            
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded){
                
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
                
            }
            else{
                
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++){
                
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if (currentlyExpanded){
                
                [tableView deleteRowsAtIndexPaths:tmpArray withRowAnimation:UITableViewRowAnimationTop];
                
                cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeDown];
                
            }
            else{
                
                [tableView insertRowsAtIndexPaths:tmpArray withRowAnimation:UITableViewRowAnimationTop];
                
                cell.accessoryView =  [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeUp];
                
            }
        }
    }
}


-(void)customFunctionForMoveScreen:(NSString *)viewcontroller{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:viewcontroller];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    [navController setViewControllers: @[rootViewController] animated: YES];
    [self.revealViewController setFrontViewController:navController];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
}


@end
