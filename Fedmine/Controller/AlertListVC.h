

#import <UIKit/UIKit.h>

@interface AlertListVC : UIViewController<UITableViewDataSource,
UITableViewDelegate>{
    
    IBOutlet UITableView *myTableView;
    NSArray *myData;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
