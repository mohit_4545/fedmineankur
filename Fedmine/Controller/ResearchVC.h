//
//  ResearchVC.h
//  Fedmine
//
//  Created by Mohit Gupta on 27/08/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResearchVC : UIViewController<UITableViewDataSource,
UITableViewDelegate>{
    
    IBOutlet UITableView *myTableView;
    NSMutableArray *myData;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
