
#import "registrationVC.h"
#import <Foundation/Foundation.h>
@interface registrationVC ()
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;
@end

@implementation registrationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 600);
    [_activityIndicatorView setHidden:true];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)registrationClick:(id)sender{
    
    if ([self.txtName.text isEqualToString:@""]) {
        [self showMessage:@"Error" withTitle:@"Fill Name here"];
    }
    else if ([self.txtEmail.text isEqualToString:@""]){
        [self showMessage:@"Error" withTitle:@"Fill Email here"];
    }
    else if ([self.txtPhone.text isEqualToString:@""]){
        [self showMessage:@"Error" withTitle:@"Fill Phone number here"];
    }
    else if ([self.txtSubject.text isEqualToString:@""]){
        [self showMessage:@"Error" withTitle:@"Fill Subject here"];
    }
    else if ([self.txtAddress.text isEqualToString:@""]){
        [self showMessage:@"Error" withTitle:@"Fill Address here"];
    }
    else if ([self.txtCity.text isEqualToString:@""]){
        [self showMessage:@"Error" withTitle:@"Fill City here"];
    }
    else if ([self.txtState.text isEqualToString:@""]){
        [self showMessage:@"Error" withTitle:@"Fill Subject here"];
    }
    else if ([self.txtCountry.text isEqualToString:@""]){
        [self showMessage:@"Error" withTitle:@"Fill Pincode here"];
    }
    else{
        [self registerationAPI];
    }
}

-(IBAction)backClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)registerationAPI{
    
    
    _activityIndicatorView.transform = CGAffineTransformMakeScale(2.0, 2.0);
    [_activityIndicatorView setHidden:false];
    [_activityIndicatorView startAnimating];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"902dd746-36c8-a4f4-18b1-6055fb781383" };
    NSDictionary *parameters = @{@"name":_txtName.text,@"email":_txtEmail.text,@"phone":_txtPhone.text,@"subject":_txtSubject.text,@"address":_txtAddress.text,@"city":_txtCity.text,@"state":_txtState.text,@"country":_txtCountry.text,@"zip":_txtzipcode.text};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://54.225.94.187/fedconnect_bizopps/api/signup_request"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"%@", httpResponse);
        
        
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSData * responseData = [requestReply dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showMessage:[jsonDict valueForKey:@"message"] withTitle:@""];
            
            if (error) {
                
                NSLog(@"%@", error);
                
            } else {
                NSLog(@"requestReply: %@", jsonDict);
            }
            
            _activityIndicatorView.transform = CGAffineTransformIdentity;
            [self.activityIndicatorView stopAnimating];
            [_activityIndicatorView setHidden:true];

        });
      }];
    [dataTask resume];
}


-(void)showMessage:(NSString*)message withTitle:(NSString *)title{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [[self view] endEditing:TRUE];
    
}

@end
