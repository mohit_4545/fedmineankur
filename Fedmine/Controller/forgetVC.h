

#import <UIKit/UIKit.h>

@interface forgetVC : UIViewController<NSURLConnectionDataDelegate,UITextFieldDelegate>{
    
    NSMutableData *_responseData;
    
}

@property(nonatomic, retain) IBOutlet UITextField *txtUsername;
@property(nonatomic, retain) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end
