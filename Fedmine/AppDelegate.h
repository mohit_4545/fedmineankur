//
//  AppDelegate.h
//  Fedmine
//
//  Created by Mohit Gupta on 25/07/17.
//  Copyright © 2017 Mohit Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

